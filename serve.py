#!/usr/bin/env python

from pathlib import Path

from livereload import Server

base_dir = Path(__file__).parent.resolve()

server = Server()
server.watch(str(base_dir / '*.html'))
server.watch(str(base_dir / 'js' / '*.js'))
server.watch(str(base_dir / 'css' / '*.css'))

server.serve(host='0.0.0.0', liveport=35729)
